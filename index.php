
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Efes</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="description" content="The most modern landingpage builder and conversion suite ⏤ now with a lifetime deal!">
    <meta name="keywords" content="fastpages">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="Lifetime deal! | FastPages" />
    <meta property="og:description" content="The most modern landingpage builder and conversion suite ⏤ now with a lifetime deal!" />
    <meta property="og:image" content="" />
   <link rel="shortcut icon" href="assets/img/favicon/efes-icon.ico" />
    <link rel="icon" type="image/png" href="https://convertpage.io/wp-content/uploads/2018/01/Light-1.png">
    <link rel="stylesheet" href="https://d1zviajkun9gxg.cloudfront.net/content/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://d1zviajkun9gxg.cloudfront.net/content/vendor/fontawesome/css/fontawesome-5.min.css" />
    <link href="assets/css/style1.css" rel="stylesheet">
    
    <link rel="stylesheet" href="https://d1zviajkun9gxg.cloudfront.net/content/general/campaignStyle.css">
   <link rel="stylesheet" href="assets/css/footerfix.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style type="text/css">
        .campaign-general-color-background {
            background-color: #999999;
        }

        .campaign-general-color-border {
            border-color: #999999;
        }

        .campaign-general-color-text {
            color: #999999;
        }

        .ck h1,
        .ck h2,
        .ck h3,
        .ck h4,
        .ck h5 {
            font-family: montserrat;
        }

        .ck p {
            font-family: lato;
        }
    </style>
    <style type="text/css">
    @font-face {
        font-family: MuseoSansRounded;
        font-weight: 300;
        src: url(https://d1zviajkun9gxg.cloudfront.net/content/fonts/museosansrounded/MuseoSansRounded-300.eot);
        src: url(https://d1zviajkun9gxg.cloudfront.net/content/fonts/museosansrounded/MuseoSansRounded-300.woff2) format('woff2'), url(https://d1zviajkun9gxg.cloudfront.net/content/fonts/museosansrounded/MuseoSansRounded-300.woff) format('woff'), url(https://d1zviajkun9gxg.cloudfront.net/content/fonts/museosansrounded/MuseoSansRounded-300.ttf) format('truetype')
    }
    
    @font-face {
        font-family: MuseoSansRounded;
        font-weight: 500;
        src: url(https://d1zviajkun9gxg.cloudfront.net/content/fonts/museosansrounded/MuseoSansRounded-500.eot);
        src: url(https://d1zviajkun9gxg.cloudfront.net/content/fonts/museosansrounded/MuseoSansRounded-500.woff2) format('woff2'), url(https://d1zviajkun9gxg.cloudfront.net/content/fonts/museosansrounded/MuseoSansRounded-500.woff) format('woff'), url(https://d1zviajkun9gxg.cloudfront.net/content/fonts/museosansrounded/MuseoSansRounded-500.ttf) format('truetype')
    }
    
    @font-face {
        font-family: MuseoSansRounded;
        font-weight: 700;
        src: url(https://d1zviajkun9gxg.cloudfront.net/content/fonts/museosansrounded/MuseoSansRounded-700.eot);
        src: url(https://d1zviajkun9gxg.cloudfront.net/content/fonts/museosansrounded/MuseoSansRounded-700.woff2) format('woff2'), url(https://d1zviajkun9gxg.cloudfront.net/content/fonts/museosansrounded/MuseoSansRounded-700.woff) format('woff'), url(https://d1zviajkun9gxg.cloudfront.net/content/fonts/museosansrounded/MuseoSansRounded-700.ttf) format('truetype')
    }
    
    body,
    h1,
    h2,
    h3,
    h4,
    html,
    p,
    span,
    strong {
        font-family: MuseoSansRounded, 'Open Sans', sans-serif!important;
    }
    section[data-id="f27ab6d1-0fe8-48bb-b8c5-35c3c246f4af"] .text-icon-inline i.icon {
        width: 58px;
        text-align: center;
    }
    section[data-id="990a5aca-4e6e-49db-a869-2ec967807dff"] .text-icon-content {
        line-height: 1;
    }
    section[data-id="9a6056f6-62e3-414e-bcf4-163a60114f56"] .section-people-two-box {
        background: #fff;
    }
    section[data-id="990a5aca-4e6e-49db-a869-2ec967807dff"] .section-text-two-box {
        background: #565d39;
    }
    section[data-id="990a5aca-4e6e-49db-a869-2ec967807dff"] .section-text-two-box:before {
        border-right-color: #565d39;
    }
    @media (max-width: 992px) {
        section[data-id="990a5aca-4e6e-49db-a869-2ec967807dff"] .section-text-two-box:before {
            border-bottom-color: #565d39;
            border-right-color: transparent;
        }
    }
    
    .comparison-mobile {
        display: none!important;
    }
    .comparison-desktop {
        display: block!important;
    }
    @media (max-width: 992px) {
        .comparison-mobile {
            display: block!important;
        }
        .comparison-desktop {
            display: none!important;
        }
    }
    
    @media (max-width: 992px) {
        section[data-id="f27ab6d1-0fe8-48bb-b8c5-35c3c246f4af"] [class*=col-] {
            padding: 0 25px!important;
        }   
    }
    </style>
    

    
 
    <style type="text/css">
        .section-sticky-countdown {
            position: fixed;
            top: 0;
            width: 100%;
            height: 165px;
            padding: 15px 0;
            z-index: 1000;
        }
        .section-sticky-countdown .countdown {
            margin-top: 15px;
        }
        .section-sticky-countdown .countdown .countdown-block {
            padding: 0;
        }
        .section-holder {
            padding-top: 165px;
        }
        @media (max-width: 992px) {
            .section-sticky-countdown {
                height: 135px;
            }
            .section-sticky-countdown [class*=col-] {
                margin: -30px 0;
            }
            .section-sticky-countdown .countdown .countdown-digit {
                font-size: 24px!important;
            }
            .section-holder {
                padding-top: 135px;
            }
        }
        .sticky-fix {
            display: none;
        }
        .claim{
            width:auto;
            padding:20px 80px;
            border-color:#3d4128;
            border-radius:6px;
            background:#3d4128;
            display:block;
            margin:0 auto;
            color: white;
        }
        .claim:hover {
  background-color: #1ABC9C; /* Green */
  border-color:#1ABC9C;
  color: white;
}

.title1{
        text-align: center;
        padding-bottom: 20px;
}

.wa_btn {
    background-color: green;
    margin: auto;
    padding: 10px 10px 10px;
    color: white;
    border-radius: 8px;



}

.whatsapp-box{
        position: fixed;
        bottom:50px;
        right:15px;
        z-index: 99;
        background: #fff;
        width: 350px;
        border-radius: 10px;
        overflow: hidden;
        box-shadow: 0px 1px 10px rgba(0,0,0,0.1);
        transform: translateY(100%);
        opacity: 0;
        visibility: hidden;
        transition: .5s cubic-bezier(0.175, 0.885, 0.32, 1.275);
    }
    .whatsapp-box.active{
        transform: translateY(0);
        visibility: visible;
        opacity: 1;
    }
    .whatsapp-box .welcome-text svg{
        width: 20px;
        height: auto;
    }
    .whatsapp-box .welcome-text svg{
        height: 18px;
        width: auto;
        fill:#aaa;
        vertical-align: middle;
    }
    .whatsapp-box .welcome-text .time{
        font-size: 12px;
        color:#aaa;
    }
    .whatsapp-box .whatsapp-form svg{
        height: 20px;
        width: auto;
        fill:#aaa;
    }
    .whatsapp-box .heading{
        background:linear-gradient(#2ecc71,#1ABF60);
        padding:30px;
        color:#fff;
        text-align: center;
        position: relative;
    }
    .whatsapp-box .heading .btn-close{
        background: none;
        border:none;
        outline: none;
        box-shadow: none;
        position: absolute;
        top:0;
        left:0;
        color:#fff;
        padding:15px 30px;
        font-size: 20px;
    }
    .whatsapp-box .heading .avatar{
        width: 80px;
        height: 80px;
        background: #575d39;
        border-radius: 50%;
        overflow: hidden;
        margin:0 auto 10px;
    }
    .whatsapp-box .heading .avatar img{
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    .whatsapp-box .chatbox{
        padding:15px;
        background: #eee;
    }
    .whatsapp-box .whatsapp-form{
        display: flex;
        flex-direction: row;
        flex:1;
        padding:10px;
    }
    .whatsapp-box .whatsapp-form .form-control{
        width: 100%;
        border:none;
        box-shadow: none;
        background: none;
    }
    .whatsapp-box .whatsapp-form .btn-send{
        width: auto;
        background: none;
        border:none;
        outline: none;
    }
    .whatsapp-box .welcome-text{
        background: #E4FDC7;
        font-size: 14px;
        position: relative;
        padding:15px;
        box-shadow: 0px 1px 3px rgba(0,0,0,0.1);
        border-radius: 5px;
        font-family: "roboto", sans-serif;
        margin-left:15px;
        line-height: 120%;
        transform: translateY(50px);
        opacity: 0;
        visibility: hidden;
        transition: .3s ease;
        transition-delay: .6s;
    }
    .whatsapp-box.active .welcome-text{
        opacity: 1;
        visibility: visible;
        transform: translateY(0);
    }
    .whatsapp-box .welcome-text:before{
        content: "";
        position: absolute;
        top:0;
        left:-16px;
        border:8px solid transparent;
        border-right: 8px solid #E4FDC7;
        border-top: 8px solid #E4FDC7;
    }
    .whatsapp-box .welcome-text .notice{
        float: right;
        margin:5px 0 0;
    }
    @media (max-width: 767px){
        .welcome-tip{
            display: none;
        }
        .btn-whatsapp{
            width: auto;
            left:0;
            right:0;
            border-radius: 0;
        }
        .whatsapp-box{
            top:0;
            left:0;
            bottom:0;
            right:0;
            z-index: 99999;
            width: auto;
            background: #eee;
            border-radius: 0;
            transition: .5s ease;
        }
        .whatsapp-form{
            position: absolute;
            bottom:0;
            left:0;
            right:0;
            box-shadow: 0px -10px 50px -15px rgba(0,0,0,0.1);
            background: #fff;
        }
    }

    </style>



</head>
<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N2XXWND"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
   <div data-server-rendered="true" class="main-container">
    <div class="section-holder">
        <div>
            <section data-id="2c2cc29a-1b63-49e3-b9f7-a8a3a393dada" class="header section-blank section-sticky-countdown">
                <div class="layout-media">
                    <div data-bg="" class="layout-media-image lazyload"></div>
                    <div class="layout-media-video"></div>
                </div>
                <div class="layout-color" style="background-color:rgba(235,9, 9, 1);"></div>
                <div class="layout-content" style="padding:0px 0px 0px 0px ;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div>
                                    <div class="ck ck-editor-project">
                                        <div>
                                            <p style="text-align:center;"><span class="text-big"><span style="color:hsl(0,0%,100%);"><strong>This special deal ends in:</strong></span></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="df07ef08-68ac-4961-a404-dd16b9b3ddbe" data-deadline="1563487200000" date-cookie-timer="true" date-cookie-days="1" date-cookie-hours="1" class="countdown" style="color:rgba(255,255,255,1);">
                                    <div class="countdown-inner">
                                        <div class="countdown-block days"><div class="countdown-digit"><b>0</b></div> <span>
                                          Days
                                      </span></div>
                                      <div class="countdown-block hours">
                                            <div class="countdown-digit"><b class="hours-selector">0</b></div><span>Hours</span></div>
                                        <div class="countdown-block minutes">
                                            <div class="countdown-digit"><b class="minutes-selector">0</b></div><span>Minutes</span></div>
                                        <div class="countdown-block seconds">
                                            <div class="countdown-digit"><b class="seconds-selector">0</b></div><span>Seconds</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="sticky-fix" style="visibility: hidden"></div>
        </div>

        <div>
            <section data-id="248e1afa-4a74-4e2f-b0e3-0fc8ac3939e8" class="header section-blank">
                <div class="layout-media">
                    <div data-bg="" class="layout-media-image lazyload"></div>
                    <div class="layout-media-video"></div>
                </div>
                <div class="layout-color" style="background-color:#565d39;"></div>
                <div class="layout-content" style="padding:50px 0px 50px 0px ;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div><img src="assets/img/logo/Logo-Efes-2.png" data-href="" class="image-element lazyload" style="display:block;margin-top:-30px;margin-left:auto;margin-right: auto;width:20%;"></div><br>
                                <div>
                                    <div class="ck ck-editor-project">
                                        <div>
                                            <h2 style="text-align:center; color: white;"><span class="text-big"><strong>How does FastPages compare?</strong></span></h2>
                                            <p style="text-align:center;">&nbsp;</p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div><img data-src="https://d1zviajkun9gxg.cloudfront.net/user/prod/2019/07/12/fastpages-af4636e1-6a62-4717-8fe3-c687dbe4914b.png" data-href="" class="image-element lazyload comparison-desktop" style="display:block;margin:0 auto;width:100%;"></div>
                                </div>
                                <div>
                                    <div><img src="assets/img/paket/efes-1.png" style="display:block;margin:0 auto;width:100%;
                                        border-radius: 4px;
                                        padding: 5px;
                                        width: 3500x;
                                        background-color: #575d39;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="sticky-fix" style="visibility: hidden"></div>
        </div>
        <div>
            <section data-id="9a6056f6-62e3-414e-bcf4-163a60114f56" class="header section-people-two">
                <div class="layout-media">
                    <div data-bg="" class="layout-media-image lazyload"></div>
                    <div class="layout-media-video"></div>
                </div>
                <div class="layout-color" style="background-color:#b8bd9d;"></div>
                <div class="layout-content" style="padding:50px 0px 90px 0px ;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    <div class="ck ck-editor-project">
                                        <div>
                                            <h2 style="text-align:center;"><span class="text-big"><strong>What others say about us!</strong></span></h2>
                                            <p style="text-align:center;">&nbsp;</p>
                                            <p style="text-align:center;"><span class="text-big"><span style="color:hsl(0,0%,30%);">Our happy customers use FastPages to create landingpages,</span></span>
                                            </p>
                                            <p style="text-align:center;"><span class="text-big"><span style="color:hsl(0,0%,30%);">forms and funnels to convert leads into customers.</span></span>
                                            </p>
                                            <p style="text-align:center;">&nbsp;</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="section-people-two-box" style="background-color:#b8bd9d;">
                                    <div class="text-icon-container ">
                                        <div class="text-icon">
                                            <div class="text-icon-inline">
                                                <div><i class="campaign-general-color-text icon fas fa-quote-left" style="color:rgba(247,118,83,1);font-size:30px;"></i></div>
                                            </div>
                                            <div class="text-icon-content">
                                                <div>
                                                    <div class="ck ck-editor-project">
                                                        <div>
                                                            <p><span style="color:hsl(0,0%,30%);">I've seen my leads being doubled since we're using FastPages. I told my entire network about this. I personally really adore using FastPages because it's incredibly simple and fun.</span></p>
                                                            <p>&nbsp;</p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div>
                                        <img src="assets/img/dr-fredy.jpg" data-href="" class="image-element lazyload" style="display:block;margin-top:-50px;margin-bottom: 50%;margin-left:auto;margin-right:auto;width:80%;">
                                    </div>
                                    
                                    <div style="margin-bottom: -120px;">
                                        <div class="ck ck-editor-project">
                                            <div>
                                                <h4 style="text-align:center;"><span class="text-small"><strong>Dr Fredy</strong></span></h4>
                                                <h4 style="text-align:center;"><span class="text-tiny"><i><strong>Marketeer</strong></i></span></h4>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="sticky-fix" style="visibility: hidden"></div>
        </div>
        <div>
            <section data-id="e04b3ab7-358f-4f6a-b554-797da07a8f3c" class="header section-blank">
                <div class="layout-media">
                    <div data-bg="" class="layout-media-image lazyload"></div>
                    <div class="layout-media-video"></div>
                </div>
                <div class="layout-color" style="background-color: #8e9974 !important;"></div>
                <div class="layout-content" style="padding:50px 0px 50px 0px ;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    <div><img data-src="https://d1zviajkun9gxg.cloudfront.net/user/prod/2019/06/28/fastpages-ac5366da-0625-42c2-80c9-983f6ab75e1b.png" data-href="" class="image-element lazyload" style="display:block;margin:0 auto;width:20%;"></div>
                                </div>
                                <div>
                                    <div class="ck ck-editor-project">
                                        <div>
                                            <h2 style="text-align:center;">&nbsp;</h2>
                                            <h2 style="text-align:center;"><span class="text-big"><span style="color:hsl(0,0%,100%);"><strong>Faster Pages, Higher conversions!</strong></span></span>
                                            </h2>
                                            <h4 style="text-align:center;">&nbsp;</h4>
                                            <p style="text-align:center;"><span class="text-huge"><span style="color:hsl(0,0%,100%);">Build ultra fast landing pages in seconds and convert more visitors into leads at lightning speed.</span></span>
                                            </p>
                                            <h4 style="text-align:center;">&nbsp;</h4>
                                            <h4 style="text-align:center;"><span style="color:rgb(255,255,255);"><strong>From $</strong></span><s><span style="color:rgb(255,255,255);"><strong>1908,-</strong></span></s></h4>
                                            <h4 style="text-align:center;"><span style="font-size:28px;"><span style="color:hsl(0,0%,100%);"><strong>Only $97,- once!</strong></span></span>
                                            </h4>
                                            <p style="text-align:center;"><span style="color:hsl(0,0%,100%);"><strong>(30-day money back guarantee!)</strong></span></p>
                                            <p style="text-align:center;">&nbsp;</p>
                                            <p style="text-align:center;">&nbsp;</p>
                                        </div>
                                    </div>
                                </div>
                                <div id="" ><a class="wa_btn claim" style="background-color:#8e9974;"><button id=""  data-button="button" class="claim"><div><div class="ck ck-editor-project"><div><p><span style="color:hsl(0,0%,100%);"><strong>CLAIM DEAL</strong></span></p></div></div></div></button></a></div>
                                <div
                                    style="height:40px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="sticky-fix" style="visibility: hidden"></div>
        </div>
        <div>
            <section data-id="990a5aca-4e6e-49db-a869-2ec967807dff" class="header section-text-two">
                <div class="layout-media">
                    <div data-bg="" class="layout-media-image lazyload"></div>
                    <div class="layout-media-video"></div>
                </div>
                <div class="layout-color" style="background-color:#e1e1e1;"></div>
                <div class="layout-content" style="padding:50px 0px 50px 0px ;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5">
                                <div>
                                    <div class="title1">
                                        <div>
                                            <h2><span class="text-big"><strong>Temporary</strong></span></h2>
                                            <h2><span class="text-big"><strong>lifetime deal!</strong></span></h2>
                                            <p>&nbsp;</p>
                                            <h4><span style="color:hsl(0,0%,0%);"><strong>From $</strong></span><s><span style="color:hsl(0,0%,0%);"><strong>1908,-</strong></span></s></h4>
                                            <p><span style="font-size:28px;"><span style="color:hsl(0,0%,0%);"><strong>Only $97,- once!</strong></span></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div><img src="assets/img/satuan/product2.png" style="display:block;margin:0 auto;width:100%;
                                    border-radius: 4px;
                                    padding: 5px;
                                    width: 300px;
                                    background-color: #e1e1e1;"></div>

                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-6 section-text-two-box">
                                <div style="height:40px;"></div>
                                <div>
                                    <div class="ck ck-editor-project">
                                        <div>
                                            <h4 style="text-align:center;"><span style="color:hsl(0,0%,100%);">Be smart, grab lifetime while it's still available.</span></h4>
                                            <p style="text-align:center;">&nbsp;</p>
                                        </div>
                                    </div>
                                </div>
                                <div data-id="c8e13a66-0be6-415a-bcd9-7ca63ddeedba" data-deadline="1563487200000" date-cookie-timer="true" date-cookie-days="1" date-cookie-hours="1" class="countdown" style="color:rgba(255,255,255,1);">
                                    <div class="countdown-inner">
                                        <div class="countdown-block days">
                                            <div class="countdown-digit"><b class="days-selector">0</b></div><span>Days</span></div>
                                        <div class="countdown-block hours">
                                            <div class="countdown-digit"><b class="hours-selector">0</b></div><span>Hours</span></div>
                                        <div class="countdown-block minutes">
                                            <div class="countdown-digit"><b class="minutes-selector">0</b></div><span>Minutes</span></div>
                                        <div class="countdown-block seconds">
                                            <div class="countdown-digit"><b class="seconds-selector">0</b></div><span>Seconds</span></div>
                                    </div>
                                </div>
                                <div style="height:40px;"></div>
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-check-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,100%);">Publish <strong>UNLIMITED</strong> pages</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-check-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,100%);">20.000 visitors / month</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-check-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,100%);">Connect 3 domains&nbsp;</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-check-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,100%);">Including SSL certificates</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-check-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="font-size:18px;"><span style="color:hsl(0,0%,100%);">All templates &amp; widgets included</span></span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-check-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,100%);">Urgency pages</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-check-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,100%);">Add custom code</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-check-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,100%);">WordPress plugin</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-check-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,100%);">Converting quizzes (coming soon)</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-check-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,100%);">Payments integration (coming soon)</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-check-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,100%);"><strong>30-Day Money Back Guarantee</strong></span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="height:40px;"></div>
                                <div>
                                    <div class="ck ck-editor-project">
                                        <div>
                                            <h4 style="text-align:center;"><span style="color:rgb(255,255,255);"><strong>From $</strong></span><s><span style="color:rgb(255,255,255);"><strong>1908,-</strong></span></s></h4>
                                            <p style="text-align:center;"><span style="font-size:28px;"><span style="color:hsl(0,0%,100%);"><strong>Only $97,- once!</strong></span></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div id=""><a class="wa_btn claim" style="background-color:#565d39;"><button id="" data-src="index.html" data-button="button" class="claim"><div><div class="ck ck-editor-project"><div><p><span style="color:hsl(0,0%,100%);"><strong>CLAIM DEAL!</strong></span></p></div></div></div></button></a></div>
                                <div
                                    style="height:40px;"></div>
                        </div>
                    </div>
                </div>
        </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
    <div>
        <section data-id="afd91eab-b348-480f-92e5-5c245e24141f" class="header section-blank">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:#e1e1e1;"></div>
            <div class="layout-content" style="padding:50px 0px 50px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        <h3><strong>Slow pages will kill your conversion rate. With FastPages you don't have to worry about that anymore.&nbsp;</strong></h3>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        <p><span style="color:hsl(0,0%,60%);">Do you know what is one of the key factors of conversion rates?</span></p>
                                        <p>&nbsp;</p>
                                        <p><span style="color:hsl(0,0%,60%);"><strong>It is website speed!</strong></span></p>
                                        <p>&nbsp;</p>
                                        <p><span style="color:hsl(0,0%,60%);">If you have a loading speed higher than 3 seconds, your conversion rate drops by a whopping 25%.</span></p>
                                        <p>&nbsp;</p>
                                        <p><span style="color:hsl(0,0%,60%);">So, chances are you’re wasting money everyday. FastPages.io gives anyone the ability to quickly build ultra fast high-converting landing pages without having to hire outside help. (No coding knowledge is required to use FastPages.io.)</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div>
                                <div><img data-src="https://d1zviajkun9gxg.cloudfront.net/user/prod/2019/07/11/fastpages-c669594a-c911-470c-a83a-ce0ef3cab0a3.jpeg" data-href="" class="image-element lazyload" style="display:block;margin:0 auto;width:80%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
    <div>
        <section data-id="f27ab6d1-0fe8-48bb-b8c5-35c3c246f4af" class="header section-cover-ten">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:#565d39;"></div>
            <div class="layout-content" style="padding:50px 0px 50px 0px ;">
                <div class="container">
                    <div class="row dark-background">
                        <div class="col-md-12">
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        <h2 style="text-align:center;"><span class="text-big"><span style="color:white;"><strong>Many useful features are included.</strong></span></span>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row dark-background">
                        <div class="col-md-4 section-cover-ten-box">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-laptop-code" style="color:#e1e1e1;font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h4><span class="text-small"><span style="color:white;"><strong>INSERT CUSTOM SCRIPTS</strong></span></span>
                                                    </h4>
                                                    <p>&nbsp;</p>
                                                    <p><span style="color:white;">Enable 3th-party integrations such as Google Analytics or embed your own code, forms, or anything you like.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 section-cover-ten-box">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fab fa-wordpress" style="color:#e1e1e1;font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h4><span class="text-small"><span style="color:white;"><strong>WORDPRESS INTEGRATION</strong></span></span>
                                                    </h4>
                                                    <p>&nbsp;</p>
                                                    <p><span style="color:white;">Easily connect your pages to an exisiting WordPress-website, with just a single click. Choose your own slug.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 section-cover-ten-box">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-user-clock" style="color:#e1e1e1;font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h4><span class="text-small"><span style="color:white;"><strong>URGENCY TIMERS</strong></span></span>
                                                    </h4>
                                                    <p>&nbsp;</p>
                                                    <p><span style="color:white;">Attach countdowns to your page to create urgency, which'll help triggering your visitors to do your desired action.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row dark-background">
                        <div class="col-md-4 section-cover-ten-box">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-external-link-alt" style="color:#e1e1e1;font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h4><span class="text-small"><span style="color:white;"><strong>CONNECT TO 1000+ APPS</strong></span></span>
                                                    </h4>
                                                    <p>&nbsp;</p>
                                                    <p><span style="color:white;">Forms can forward leads to other (marketing) services like MailChimp, ActiveCampaign and many more. Fully automatical.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 section-cover-ten-box">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon far fa-id-card" style="color:#e1e1e1;font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h4><span class="text-small"><span style="color:white;"><strong>GATHER LEADS EASILY</strong></span></span>
                                                    </h4>
                                                    <p>&nbsp;</p>
                                                    <p><span style="color:white;">Create beautiful forms for your pages to obtain leads and start interactingwith your potential customers.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 section-cover-ten-box">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-toolbox" style="color:#e1e1e1;font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h4><span class="text-small"><span style="color:white;"><strong>SIMPLE ONLINE EDITOR</strong></span></span>
                                                    </h4>
                                                    <p>&nbsp;</p>
                                                    <p><span style="color:white;">We've created the most simple web-based editor that comes with dozens of options and tools to build your perfect page.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row dark-background">
                        <div class="col-md-4 section-cover-ten-box">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-rocket" style="color:#e1e1e1;font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h4><span class="text-small"><span style="color:white;"><strong>CREATE ULTRA-FAST PAGES</strong></span></span>
                                                    </h4>
                                                    <p>&nbsp;</p>
                                                    <p><span style="color:white;">Graphs &amp; tests prove that we're the fastest in the competition. No more frustrated visitors due to slow loading times.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 section-cover-ten-box">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon far fa-flag" style="color:#e1e1e1;font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h4><span class="text-small"><span style="color:white;"><strong>USE OWN DOMAINS</strong></span></span>
                                                    </h4>
                                                    <p>&nbsp;</p>
                                                    <p><span style="color:white;">Rather use your own domain? Sure! Simply connect it with a single DNS-record. We even provide you free SSL.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 section-cover-ten-box">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-brush" style="color:#e1e1e1;font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h4><span class="text-small"><span style="color:white;"><strong>CONSISTENT STYLING</strong></span></span>
                                                    </h4>
                                                    <p>&nbsp;</p>
                                                    <p><span style="color:white;">We help you to make sure your styling looks great, we synchronize your styling across your entire campaign.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
    <div>
        <section data-id="9a6056f6-62e3-414e-bcf4-163a60114f56" class="header section-people-two">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:#ffffff;"></div>
            <div class="layout-content" style="padding:50px 0px 90px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        <h2 style="text-align:center;"><span class="text-big"><strong>What others say about us!</strong></span></h2>
                                        <p style="text-align:center;">&nbsp;</p>
                                        <p style="text-align:center;"><span class="text-big"><span style="color:hsl(0,0%,30%);">Our happy customers use FastPages to create landingpages,</span></span>
                                        </p>
                                        <p style="text-align:center;"><span class="text-big"><span style="color:hsl(0,0%,30%);">forms and funnels to convert leads into customers.</span></span>
                                        </p>
                                        <p style="text-align:center;">&nbsp;</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="section-people-two-box">
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-quote-left" style="color:rgba(247,118,83,1);font-size:30px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,30%);">FastPages is our new secret weapon. It delivers on its promise of being the fastest loading landingpage builder on the market. It helps us build high converting pages in minutes.</span></p>
                                                        <p>&nbsp;</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="ck ck-editor-project">
                                        <div>
                                            <h4 style="text-align:center;"><span class="text-small"><strong>Matt Miciula</strong></span></h4>
                                            <h4 style="text-align:center;"><span class="text-tiny"><i><strong>CEO</strong></i></span></h4>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div><img src="assets/img/profil/pf1.png" data-href="" class="image-element lazyload" style="display:block;margin:-30px auto;width:50%;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="section-people-two-box">
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-quote-left" style="color:rgba(247,118,83,1);font-size:30px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,30%);">I've seen my leads being doubled since we're using FastPages. I told my entire network about this. I personally really adore using FastPages because it's incredibly simple and fun.</span></p>
                                                        <p>&nbsp;</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="ck ck-editor-project">
                                        <div>
                                            <h4 style="text-align:center;"><span class="text-small"><strong>Mark Bongers</strong></span></h4>
                                            <h4 style="text-align:center;"><span class="text-tiny"><i><strong>Marketeer</strong></i></span></h4>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div><img src="assets/img/profil/pf1.png" data-href="" class="image-element lazyload" style="display:block;margin:-30px auto;width:50%;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="section-people-two-box">
                                <div class="text-icon-container ">
                                    <div class="text-icon">
                                        <div class="text-icon-inline">
                                            <div><i class="campaign-general-color-text icon fas fa-quote-left" style="color:rgba(247,118,83,1);font-size:30px;"></i></div>
                                        </div>
                                        <div class="text-icon-content">
                                            <div>
                                                <div class="ck ck-editor-project">
                                                    <div>
                                                        <p><span style="color:hsl(0,0%,30%);">Thanks to those amazing blueprints, I was able to built beautiful landingpages all by myself, while I'm not a designer at all! The editor is simple to use, keep up the good work.</span></p>
                                                        <p>&nbsp;</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="ck ck-editor-project">
                                        <div>
                                            <h4 style="text-align:center;"><span class="text-small"><strong>Inge Mennen</strong></span></h4>
                                            <h4 style="text-align:center;"><span class="text-tiny"><i><strong>Marketeer</strong></i></span></h4>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div><img src="assets/img/profil/pf1.png" data-href="" class="image-element lazyload" style="display:block;margin:-30px auto;width:50%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
    <div>
        <section data-id="248e1afa-4a74-4e2f-b0e3-0fc8ac3939e8" class="header section-blank">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:#565d39;"></div>
            <div class="layout-content" style="padding:50px 0px 50px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        <h2 style="text-align:center; color: white;"><span class="text-big"><strong>How does FastPages compare?</strong></span></h2>
                                        <p style="text-align:center;">&nbsp;</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div><img data-src="https://d1zviajkun9gxg.cloudfront.net/user/prod/2019/07/12/fastpages-af4636e1-6a62-4717-8fe3-c687dbe4914b.png" data-href="" class="image-element lazyload comparison-desktop" style="display:block;margin:0 auto;width:100%;"></div>
                            </div>
                            <div>
                                <div><img src="assets/img/paket/efes-1.png" style="display:block;margin:0 auto;width:100%;
                                    border-radius: 4px;
                                    padding: 5px;
                                    width: 3500x;
                                    background-color: #575d39;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
    <div>
        <section data-id="274f6242-f413-4878-b486-fe8ccdaa4d73" class="header section-blank">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:rgba(255,255, 255, 1);"></div>
            <div class="layout-content" style="padding:50px 0px 50px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div>
                                <div><img data-src="https://d1zviajkun9gxg.cloudfront.net/user/prod/2019/07/03/fastpages-13773567-0788-4d31-8cbf-8fa02bd61684.jpeg" data-href="" class="image-element lazyload" style="display:block;margin:0 auto;width:80%;"></div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        <h3 class="heading text-center"><strong>2 PRODUK HANYA:</strong></h3>
                <h2 class="heading text-center"><strong>Rp.236.900</strong></h2>
                <h3 class="heading text-center"><strong><font color="#ffb00d">Gratis Membership di Aplikasi Klinik Online!</font></strong></h3>

                <h3 align="center">Bisa Bayar Ditempat!<br><strong>Cash on Delivery (COD)</strong></h3>

                <h4 align="center"><strong>MAU ORDER PAKET SKINCARE HAYYANA SEKARANG?</strong></h4>

                 <p align="center"><img src="assets/img/red-arrow.gif">
                    </p>

                    <?php
                    $gid = '';
                    $gpaket = '';
                    if (isset($_GET['id'])) {
                        $gid = $_GET['id'];
                    }
                    $cs = [
                        ['name' => 'Santi', 'mobile' => '6281229414840', 'email' => 'cs2.heptaco@gmail.com'],
                        ['name' => 'Santi', 'mobile' => '6281229414840', 'email' => 'cs2.heptaco@gmail.com'],
                        ['name' => 'Santi', 'mobile' => '6281229414840', 'email' => 'cs2.heptaco@gmail.com'],
                        ['name' => 'Santi', 'mobile' => '6281229414840', 'email' => 'cs2.heptaco@gmail.com']
                    ];
                    $keys = array_keys($cs);
                    $random = $keys[array_rand($keys,1)];
                    $name = $cs[$random]['name'];
                    $mobile = $cs[$random]['mobile'];
                    $email = $cs[$random]['email'];
                    // $urls = 'https://api.whatsapp.com/send?l=id&phone='.$mobile.'&text=Aku%20mau%20order%20Paket%20Hayyana%20dong!';
                    ?>


                <div class="text-center">
                <a class="wa_btn btn"><span style="font-size: 20px;"><span style="font-size: 20px;"><i class="fa fa-whatsapp" style="font-size:20px"></i> <span style="font-size: 20px; font-weight: normal;">Order Via WA Sekarang!</span></a>
                </div>
                <!-- star new chat -->
                <div class="whatsapp-box">
    <div class="heading">
        <button class="btn-close" type="button"><i class="fa fa-angle-left"></i></button>
        <div class="avatar"><img src="assets/img/profil/profile.jpg" alt=""></div>
        <div class="name"><?php echo $name;?></div>
    </div>
    <div class="chatbox">
        <div class="welcome-text">
            Selamat <span class="greeting"></span>, saya <?php echo $name; ?> dari Hayyana. Silahkan chat saya untuk konsultasi atau order Paket Skincare Hayyana!
            <div class="notice">
                <span class="time"></span>
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="594.149px" height="594.149px" viewBox="0 0 594.149 594.149" style="enable-background:new 0 0 594.149 594.149;" xml:space="preserve">
                    <g>
                        <g id="done-all">
                        <path d="M448.8,161.925l-35.7-35.7l-160.65,160.65l35.7,35.7L448.8,161.925z M555.899,126.225l-267.75,270.3l-107.1-107.1l-35.7,35.7l142.8,142.8l306-306L555.899,126.225z M0,325.125l142.8,142.8l35.7-35.7l-142.8-142.8L0,325.125z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g> 
                    </g>
                </svg>
            </div>
        </div>
        <br>
        <div class="welcome-text">
            Kami bisa COD, kirim barang terlebih dahulu dan bayar ditempat!
            <div class="notice">
                <span class="time"></span>
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="594.149px" height="594.149px" viewBox="0 0 594.149 594.149" style="enable-background:new 0 0 594.149 594.149;" xml:space="preserve">
                    <g>
                        <g id="done-all">
                        <path d="M448.8,161.925l-35.7-35.7l-160.65,160.65l35.7,35.7L448.8,161.925z M555.899,126.225l-267.75,270.3l-107.1-107.1l-35.7,35.7l142.8,142.8l306-306L555.899,126.225z M0,325.125l142.8,142.8l35.7-35.7l-142.8-142.8L0,325.125z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g> 
                    </g>
                </svg>
            </div>
        </div>
    </div>
    <form class="whatsapp-form" action="redirect.php?urls=<?php echo $urls ?>" data-phone="<?php echo $mobile; ?>">
        <input name="wa_message" type="text" class="form-control" placeholder="Reply.." value="Aku mau order Paket REJUVENATION Hayyana dong!" autocomplete="off">
        <button class="btn btn-send" type="submit">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="535.5px" height="535.5px" viewBox="0 0 535.5 535.5" style="enable-background:new 0 0 535.5 535.5;" xml:space="preserve"><g><g id="send"><polygon points="0,497.25 535.5,267.75 0,38.25 0,216.75 382.5,267.75 0,318.75"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
            </svg>
        </button>
    </form>
</div>
                <!-- end new chat -->
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        <p>&nbsp;</p>
                                        <p><span style="color:rgb(144,153,158);">After building several landingpages ourselves, we noticed how important clean design is for your conversion rate.</span></p>
                                        <p><span style="color:rgb(144,153,158);">We hand-crafted the best, high converting templates to be successful. We're sharing our designs with you.</span></p>
                                        <p>&nbsp;</p>
                                        <p><span style="color:rgb(144,153,158);">Your campaign will be popping-out, be unique and successful.</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
        <section data-id="274f6242-f413-4878-b486-fe8ccdaa4d73" class="header section-blank">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:rgba(255,255, 255, 1);"></div>
            <div class="layout-content" style="padding:50px 0px 50px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div>
                                <div><img data-src="https://d1zviajkun9gxg.cloudfront.net/user/prod/2019/07/03/fastpages-13773567-0788-4d31-8cbf-8fa02bd61684.jpeg" data-href="" class="image-element lazyload" style="display:block;margin:0 auto;width:80%;"></div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        <h3><span class="text-big"><strong>Simple and clean templates to easily get you started.</strong></span></h3>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        <p>&nbsp;</p>
                                        <p><span style="color:rgb(144,153,158);">After building several landingpages ourselves, we noticed how important clean design is for your conversion rate.</span></p>
                                        <p><span style="color:rgb(144,153,158);">We hand-crafted the best, high converting templates to be successful. We're sharing our designs with you.</span></p>
                                        <p>&nbsp;</p>
                                        <p><span style="color:rgb(144,153,158);">Your campaign will be popping-out, be unique and successful.</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
    <div>
        <section data-id="48fff2df-3770-4757-aea2-89423367a1fc" class="header section-cover-eight">
            <div class="layout-media">
                <div data-bg="https://d1zviajkun9gxg.cloudfront.net/content/images/placeholders/sections/cover-eight/b04326fe7771db754453da17429463eb.jpg" class="layout-media-image lazyload" style="background-attachment:fixed;"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:#565d3991;"></div>
            <div class="layout-content" style="padding:50px 0px 50px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 dark-background">
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        <h2 style="text-align:center;"><span class="text-big"><span style="color:hsl(0,0%,100%);"><strong>Not yet convinced? Test it yourself today!</strong></span></span>
                                        </h2>
                                        <p style="text-align:center;">We offer a 30-day money back guarantee, no questions asked, hassle free!</p>
                                        <p style="text-align:center;">&nbsp;</p>
                                    </div>
                                </div>
                            </div>
                            <div id=""><a class="wa_btn claim" style="background-color:#9fa28e;"><button id="" data-src="https://payments.fastpages.io/?v=lifetime-deal" data-button="button" class="claim"><div><div class="ck ck-editor-project"><div><p><span class="text-big"><strong>CLAIM DEAL</strong></span></p></div></div></div></button></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
   <div class="col-md-12">
    <div class="row">
        
                    
        
                        
                        
                       
                            <img src="assets/img/pabrik/pabrik.jpg" style="width: 100%;">
                            
                        </div>
                    </div>
            
        
        <section data-id="48fff2df-3770-4757-aea2-89423367a1fc" class="header section-cover-eight">
            
            <div class="layout-color" style="background-color:#ffffff;"></div>
            <div class="layout-content" style="padding:50px 0px 50px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 dark-background">
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        
                                        <p style="text-align:center; color: black;">We offer a 30-day money back guarantee, no questions asked, hassle free!</p>
                                        <p style="text-align:center;">&nbsp;</p>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section data-id="17d9a8a0-5786-417d-80d5-95472d36da69" class="header section-blank">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:#565d39;"></div>
            <div class="layout-content" style="padding:50px 0px 0px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        <h2 style="text-align:center;"><span style="color:hsl(0,0%,100%);"><strong>Questions? We got answers!</strong></span></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
    <div>
        <section data-id="25b5c489-ef1a-4524-b6e2-1954b8e8de35" class="header section-blank">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:#565d39;"></div>
            <div class="layout-content" style="padding:50px 0px 0px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-question-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h3><span class="text-tiny"><span style="color:hsl(0,0%,100%);"><strong>What do I get buying this deal?</strong></span></span>
                                                    </h3>
                                                    <p><span style="color:hsl(0,0%,100%);">After you bought the deal you will get an e-mail from us with the link to register and a unique coupon code. Fill in your coupon to claim your lifetime deal and start making fast converting pages for your brand.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-question-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h3><span class="text-tiny"><span style="color:hsl(0,0%,100%);"><strong>How lifetime is lifetime?</strong></span></span>
                                                    </h3>
                                                    <p><span style="color:hsl(0,0%,100%);">With lifetime, we mean lifetime. Also whitin this deal we also ensure that future updates on our software are enabled for you. The only restrictions to this deal are the number of used sub-domains and the number of monthly pageviews. You can buy more deals to raise these numbers.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
    <div>
        <section data-id="911dc9e6-4347-4be0-a38b-9baca99d9948" class="header section-blank">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:#565d39;"></div>
            <div class="layout-content" style="padding:0px 0px 0px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-question-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h3><span class="text-tiny"><span style="color:hsl(0,0%,100%);"><strong>The deal is cheap, how can you offer such a cheap deal?</strong></span></span>
                                                    </h3>
                                                    <p><span style="color:hsl(0,0%,100%);">We are proud of our product and want to show the world what we have made. With this cheap deal we want to build a steady user base and gain more user insights to make our product even better now and in the future.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-question-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h3><span class="text-tiny"><span style="color:hsl(0,0%,100%);"><strong>How can FastPages be so fast compared to others?</strong></span></span>
                                                    </h3>
                                                    <p><span style="color:hsl(0,0%,100%);">We've created a unique layer on our cloud infrastructure to guarantee the fastest pages. Together with Amazon CloudFront (a global CDN), compressed images and minified page sources, your page is lightning fast.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
    <div>
        <section data-id="e4e51499-0a3f-4ad4-a356-bca5e765feba" class="header section-blank">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:#565d39;"></div>
            <div class="layout-content" style="padding:0px 0px 0px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-question-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h3><span class="text-tiny"><span style="color:hsl(0,0%,100%);"><strong>Can I get a SSL/HTTPS certificate for my campaign?</strong></span></span>
                                                    </h3>
                                                    <p><span style="color:hsl(0,0%,100%);">Yes, it's included in this deal! You're able to obtain an SSL-certificate for every domainname that has been connected to FastPages, with absolutely no costs.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-question-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h3><span class="text-tiny"><span style="color:hsl(0,0%,100%);"><strong>Will I get access to future updates of FastPages?</strong></span></span>
                                                    </h3>
                                                    <p><span style="color:hsl(0,0%,100%);">Definitely! We will always make sure your account has access to the latest features and integrations.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
    <div>
        <section data-id="b539b7c8-13a7-4791-be08-eac00330d09b" class="header section-blank">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:#565d39;"></div>
            <div class="layout-content" style="padding:0px 0px 0px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-question-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h3><span class="text-tiny"><span style="color:hsl(0,0%,100%);"><strong>Can I create a funnel or add multiple pages in a single project?</strong></span></span>
                                                    </h3>
                                                    <p><span style="color:hsl(0,0%,100%);">Absolutely! Every campaign can contain as many pages as needed, it's unlimited! With links and buttons, you're able to connect these pages together.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-question-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h3><span class="text-tiny"><span style="color:hsl(0,0%,100%);"><strong>What happens if I exceed my monthly page views?</strong></span></span>
                                                    </h3>
                                                    <p><span style="color:hsl(0,0%,100%);">We will send you a notification by e-mail when 90% of your pageviews are reached. Then you can decide to upgrade.&nbsp;</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
    <div>
        <section data-id="56e5f407-cfea-434f-b353-1932b98cc448" class="header section-blank">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:#565d39;"></div>
            <div class="layout-content" style="padding:0px 0px 0px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-question-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h3><span class="text-tiny"><span style="color:hsl(0,0%,100%);"><strong>You offer a 30 day money back guarantee, how do i get this back after i have tested and found that the product did not fit my needs?</strong></span></span>
                                                    </h3>
                                                    <p><span style="color:hsl(0,0%,100%);">It is always possible that you have other needs than we provide. We get that! The 30 money back guarantee is easy you just send us an e-mail requesting a refund. We will process your refund and confirm your e-mail. No questions asked, no hard feelings.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-icon-container ">
                                <div class="text-icon">
                                    <div class="text-icon-inline">
                                        <div><i class="campaign-general-color-text icon fas fa-question-circle" style="color:rgba(255,255,255,1);font-size:40px;"></i></div>
                                    </div>
                                    <div class="text-icon-content">
                                        <div>
                                            <div class="ck ck-editor-project">
                                                <div>
                                                    <h3><span class="text-tiny"><span style="color:hsl(0,0%,100%);"><strong>Are the pages optimized for Google ranking?</strong></span></span>
                                                    </h3>
                                                    <p><span style="color:hsl(0,0%,100%);">Yes, our technical page structure is SEO-optimized for reaching the best possible ranking in search engines like Google, Yahoo and bing.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="sticky-fix" style="visibility: hidden"></div>
    </div>
    <div>
        <section data-id="943fa1c5-778d-4fc2-a214-9d0e443769b5" class="header section-blank">
            <div class="layout-media">
                <div data-bg="" class="layout-media-image lazyload"></div>
                <div class="layout-media-video"></div>
            </div>
            <div class="layout-color" style="background-color:#565d39;"></div>
            <div class="layout-content" style="padding:50px 0px 50px 0px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <div class="ck ck-editor-project">
                                    <div>
                                        <p style="text-align:center;"><span style="color:hsl(0,0%,100%);">Copyright 2019<strong> FastPages.io </strong>| All rights reserved</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="code-container" style="width:100%;">
                                <div class="code-container-exec">
                                    <style type="text/css">

	.stripe-button-el {
      display: none;
    }

</style>
 <div class="section" id="footer">
        <div class="bawah scroll">
            <a class="wa_btn btn"><span style="font-size: 20px;"><span style="font-size: 20px;"><i class="fa fa-whatsapp" style="font-size:20px"></i> <span style="font-size: 20px; font-weight: normal;">Hubungi CS Kami Segera!</span></a>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

</div>

</div>
</div>

    <script type="text/javascript" src="https://d1zviajkun9gxg.cloudfront.net/content/vendor/jquery/jquery-3.1.1.1.min.js"></script>
    
    <script type="text/javascript" src="https://d1zviajkun9gxg.cloudfront.net/content/general/campaign.js"></script>
     <script src="assets/js/toaster.js"></script>
     <script type="text/javascript">
    $(function(){
        
        $('.wa_btn').on('click',function(){
            if($('.whatsapp-box').hasClass('active')){
                $('.whatsapp-box').removeClass('active');
            } else {
                $('.whatsapp-box').addClass('active');
                greeting();

            }
        });
        function greeting(){
            var time = new Date();
            var timeText = time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
            $('.whatsapp-box .time').text(timeText);
            setTimeout(function() { $('input[name="wa_message"]').focus() }, 1000);

            var curHr = time.getHours()

            if (curHr < 10) {
                var greeting = 'pagi';
            } else if (curHr < 15) {
                var greeting = 'siang';
            } else if (curHr < 18) {
                var greeting = 'sore';
            } else {
                var greeting = 'malam';
            }
            $('.whatsapp-box .greeting').text(greeting);
        }
        $('.whatsapp-box .btn-close').on('click',function(){
            $('.whatsapp-box').removeClass('active');
        });

        $('.whatsapp-form').on('submit',function(){
            var url;
            var phone = $(this).data('phone');
            var message = $('.whatsapp-form input[name=wa_message]').val();
            if($(window).width() > 768){
                // window.open("https://web.whatsapp.com/send?phone="+phone+"&text="+message+"");
                url = "https://web.whatsapp.com/send?phone="+phone+"&text="+message+"";
            } else {
                // window.open("https://wa.me/"+phone+"?text="+message+"");
                url = "https://wa.me/"+phone+"?text="+message+"";
            }
            window.open("redirect.php?urls="+url+"");
            return false;
        });
        var welcomeTip = localStorage.getItem("welcome-tip");
        if(welcomeTip != 'hidden'){
            $('.welcome-tip').addClass('active');
        } else {
            $('.welcome-tip').removeClass('active');
        }
        $('#welcome-tip-close').on('click',function(){
            localStorage.setItem("welcome-tip", "hidden");
            $('.welcome-tip').removeClass('active');
        });

        $(window).on('scroll',function(){
            if($(window).scrollTop() >  200){
                localStorage.setItem("welcome-tip", "hidden");
                $('.welcome-tip').removeClass('active');
            }
        });


    });
</script>
     <script type="text/javascript">
            var interval;
            var codetmpl = "<code>%codeobj%</code><br><code>%codestr%</code>";

            $(document).ready(function () {
                randomToast();

                start();
            });

            function start() {
                if (!interval) {
                    interval = setInterval(function () {
                        randomToast();
                    }, 30000);
                }
                this.blur();
            }


            function randomToast() {
                var user = new Array("1 Paket telah diorder dari Pemalang", "4 Paket telah diorder dari Cilacap", "2 Paket telah diorder dari Manado", "3 Paket telah diorder dari Samarinda", "1 Paket telah diorder dari Bogor");
                var beli = new Array("3 menit lalu.", "5 menit lalu.", "10 menit lalu.", "11 menit lalu.", "15 menit lalu.");
                var img = new Array("<img src='assets/img/_id.png'>", "<img src='assets/img/_en.png'>");
                randomimg = img[Math.floor(Math.random() * img.length)];

                random = user[Math.floor(Math.random() * user.length)];
                randombeli = beli[Math.floor(Math.random() * beli.length)];

                var description = [
                    "",
                    "",
                    "",
                ];

                var size = description.length
                var x = Math.floor(size * Math.random())


                var priority = 'success';
                var image = description[x];
                var title = random;
                var message = randombeli;

                $.toaster({
                    priority: priority,
                    image: image,
                    title: title,
                    message: message
                });
            }


            function maketoast(evt) {
                evt.preventDefault();

                var options = {
                    priority: $('#toastPriority').val() || null,
                    image: $('#toastImage').val() || null,
                    title: $('#toastTitle').val() || null,
                    message: $('#toastMessage').val() || 'A message is required'
                };

                if (options.priority === '<use default>') {
                    options.priority = null;
                }

                var codeobj = [];
                var codestr = [];

                var labels = ['message', 'image', 'title', 'priority'];
                for (var i = 0, l = labels.length; i < l; i += 1) {
                    if (options[labels[i]] !== null) {
                        codeobj.push([labels[i], "'" + options[labels[i]] + "'"].join(' : '));
                    }

                    codestr.push((options[labels[i]] !== null) ? "'" + options[labels[i]] + "'" : 'null');
                }

                if (codestr[2] === 'null') {
                    codestr.pop();
                    if (codestr[1] === 'null') {
                        codestr.pop();
                    }
                }

                codeobj = "$.toaster({ " + codeobj.join(", ") + " });"
                codestr = "$.toaster(" + codestr.join(", ") + ");"

                $('#toastCode').html(codetmpl.replace('%codeobj%', codeobj).replace('%codestr%', codestr));
                $.toaster(options);
            }
    </script>
    <script>
    $(document).ready(function() {
    
        setTimeout(function() {
        
            var cd_hours = parseInt(
                $('.section-sticky-countdown .countdown .days-selector').text()
            );
            var cd_minutes = parseInt(
                $('.section-sticky-countdown .countdown .minutes-selector').text()
            );
            var cd_seconds = parseInt(
                $('.section-sticky-countdown .countdown .seconds-selector').text()
            );
            
            if (cd_hours === 0 && cd_minutes === 0 && cd_seconds === 0) {
                
                $('.popup-close[data-id="125490cf-01d9-4665-b179-aece250dcede"]').hide();
                $('.section-holder').css({
                    'padding-top': 0
                });
                
                $('#popup-125490cf-01d9-4665-b179-aece250dcede').show();
                $('button[data-src="https://www.fastpages.io/pricing"]').prop('disabled', false);
                $('button[data-src="https://www.fastpages.io/pricing"]').css({
                    'opacity': '1',
                    'cursor': 'pointer'
                });
                
                $('.section-sticky-countdown').hide();
                
            }
        
        }, 1000);
    
    });
</script>

</body>
</html>
